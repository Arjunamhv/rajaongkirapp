from flask import Flask, request, jsonify, make_response
from config import Config
from flask_mysqldb import MySQL
import mysql.connector
import requests
import os
from dotenv import load_dotenv

load_dotenv()

app = Flask(__name__)
app.config.from_object(Config)

app.config['MYSQL_HOST'] = 'localhost'
app.config['MYSQL_USER'] = 'username'
app.config['MYSQL_PASSWORD'] = 'password'
app.config['MYSQL_DB'] = 'rajaongkirapp'
app.config['MYSQL_CURSORCLASS'] = 'DictCursor'

mysql = MySQL(app)

def authenticate(username, password):
    cur = mysql.connection.cursor()
    cur.execute("SELECT * FROM users WHERE username = %s AND password = %s", (username, password))
    user = cur.fetchone()
    cur.close()
    if user:
        return True
    else:
        return False

@app.route('/login', methods=['POST'])
def login():
    auth = request.authorization
    if not auth or not auth.username or not auth.password:
        return make_response('Could not verify', 401, {'WWW-Authenticate': 'Basic realm="Login Required"'})
    
    if authenticate(auth.username, auth.password):
        response = jsonify({'message': 'Login successful!'})
        response.set_cookie('session_id', '12345')
        return response
    
    return make_response('Could not verify', 401, {'WWW-Authenticate': 'Basic realm="Login Required"'})

@app.route('/register', methods=['POST'])
def add_user():
    data = request.get_json()
    username = data['username']
    password = data['password']

    cur = mysql.connection.cursor()
    cur.execute("INSERT INTO users (username, password) VALUES (%s, %s)", (username, password))
    mysql.connection.commit()
    cur.close()

    return jsonify({"message": "User added successfully"}), 201

@app.route('/provinsi', methods=['GET'])
def get_provinces():
    url = f"{app.config['RAJAONGKIR_BASE_URL']}/province"
    headers = {'key': app.config['RAJAONGKIR_API_KEY']}
    response = requests.get(url, headers=headers)
    return jsonify(response.json())

@app.route('/kota', methods=['GET'])
def get_cities():
    province_id = request.args.get('province_id')
    url = f"{app.config['RAJAONGKIR_BASE_URL']}/city"
    headers = {'key': app.config['RAJAONGKIR_API_KEY']}
    params = {'province': province_id} if province_id else {}
    response = requests.get(url, headers=headers, params=params)
    return jsonify(response.json())

@app.route('/ongkos-kirim', methods=['POST'])
def get_shipping_cost():
    data = request.json
    url = f"{app.config['RAJAONGKIR_BASE_URL']}/cost"
    headers = {
        'key': app.config['RAJAONGKIR_API_KEY'],
        'Content-Type': 'application/json'
    }
    response = requests.post(url, headers=headers, json=data)
    return jsonify(response.json())

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')