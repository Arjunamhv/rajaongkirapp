import os

class Config:
    RAJAONGKIR_API_KEY = os.getenv('RAJAONGKIR_API_KEY')
    RAJAONGKIR_BASE_URL = "https://api.rajaongkir.com/starter"
